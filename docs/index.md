# NSW sTGC Commissioning documentation site

All documentation about mappings, how-to guides, etc will be publish in this site. 

Sectors under Commissioning

- **A10**
- **A14**

## Low voltage control:

- Branch controler 11: A10 - date 06.10.20
- Branch controler 15: A14 - date 08.10.20

## RIM

- Branch controler 10: A10- date 02.10.20 *channels 01 and 03 only*

## Fiber mapping on Felix Card
Fiber mapping status:

- S01 - Large Sector configuration: A14
- S02 - Small Sector configuration: A10

## High Voltage Control:

At the FSM panel there are two sectors for HV:

- FSM Panel - Bracket
- S01 -  S08
- S02 - S10

## Sector situation

The test status of the different sectors can be found here:  
[https://docs.google.com/spreadsheets/d/1pjRe_sInoPYsNVWJbgmq80IvB5iv1PbESc7IBFJUM1Q/edit#gid=1877449389](https://docs.google.com/spreadsheets/d/1pjRe_sInoPYsNVWJbgmq80IvB5iv1PbESc7IBFJUM1Q/edit#gid=1877449389)




----- 

## Powered by

 * [mkdocs](https://www.mkdocs.org/)
 * [mkdocs-material](https://github.com/squidfunk/mkdocs-material)

