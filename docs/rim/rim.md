# Rim Crate

The rim crate contains the readout electronic boards used for the sTGC trigger. The is one rim crate per NSW sector and each crate hosts 10 boards in total: 8 router boards, 1 pad trigger and 1 L1DDC board.



These pages describe:

1. The full rim crate assembly procedure
2. The  rim crates status (page under construction)
3. Instructions to perform connectivity tests
