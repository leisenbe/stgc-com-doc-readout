# Rim crate connectivity tests

!!! Warning
    Always monitor temperature and do not forget to turn off the LV when the tests are done. Do not leave LV unattended 

 The original documentation  on how to run the connectivity tests can be found on gitlab: [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/pads.md](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/pads.md) 
Check the gitlab documentation for updates, this in a copy (created  22/07/2020) with a bit more explanations for the non-experts in 191. 

## Instructions

* Log in on felix5, skip the ALTI pattern login (because the partition now does it) 
  ```
  ssh -XY nswdaq@pcatlnswfelix05  
  elinkconfig_sTGC_VS
  ```

* Program the pad trigger: load the configuration bit file:
  ```
  source .bashrc 
  program_pad_trigger_fpga 
  ```
  This last command output a command to run, it should be: 
  ```
  fscajtag -d 1 -R 10 -e 2ff /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit 
  ```
  Run that command

* Start felixcore and OPC server: 
  ```
  felixcore --data-interface eno1 --elinks 737,827 
  ./OpcUaScaServer ~/public/sw/config-ttc/config-files/opc_xml/191/A14/sector_A14_Uncalibration_STG_191_connectivity.xml 
  ```

* **Temperature monitoring**: Once OPC is running start temperature monitoring and keep and eye on it.  Pad trigger temp: ~40C is normal, <60C is safe but not good and >60C ALWAYS POWER OFF PT 
  ```
JSON="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A12/padTrigger.json" 
while true; do echo $(date) $(nsw_pad_trigger --config_file $JSON --i2c_reg 1 | grep Readback); sleep 30; done
  ```

* Run partition to take data: 
  ```
  ssh –XY nswdaq@pcatlnswswrod03 
  source /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWPartitionMaker/191A-ConnectivityPads/setup_part.sh 
  ```
  (! for now? The partition name "ConnectivityTmp"  ) 


* In GUI, subscribe to information 
* To do an sTGC test run:
  ```
  is_write -p part-191A-ConnectivityPads -n Setup.NSW.calibType -t String -v sTGCPadConnectivity -i 
  ```
  `0` Otherwise the default config is for micromegas 

* Run settings:  Check "enable" and the Tier0 project name should be data test
* At `10:49:53 INFORMATION 191A-A14-Calib ers::Message NSWCalibRc::handler::End of handler `
  Stop run 

* For data analysis: 
  ```
  ssh -Y nswdaq@pcatlnswswrod03 
  ```
  Location of file: ls `/tmp/nswdaq/ `


* Known root compatibility issue to make plots, copy file to eos (temp work  around)  [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/11]( https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/11 )


---

### Latency tests:

* Run: ` is_write -p part-191A-ConnectivityPads -n Setup.NSW.calibType -t String -v sTGCPadLatency -i 0`   
  The partition should be shutdown
