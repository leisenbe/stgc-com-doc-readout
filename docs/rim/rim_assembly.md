# Rim Crate assembly instructions
The rim crate contains the readout electronic boards used for the sTGC trigger. The is one rim crate per NSW sector and each crate hosts 10 boards in total: 8 router boards, 1 pad trigger and 1 L1DDC board.

This document describes the entire rim crate assembly procedure. It can be divided into 3 steps

1. [Copper cooling plate assembly](#copper-plate-assembly)
2. [PCB assembly](#pcb-assembly)
3. [Rim crate assembly on the wheel](#rim-crate-assembly-on-the-wheel)

!!! Warning
    The electronic boards are sensitive to static electricity discharges. Before handling the electronics, please read the [section on EDS safety](#esd-safety)



---



## Copper plate assembly

### Introduction

The document describes the assembly procedure for the Rim crate copper plates.
3 kinds of plates are needed for a Rim crate

* 8 router copper boards
* 1 L1DDC copper board
* 1 Pad Trigger copper boards + pad trigger copper bar (if not already mounted)

The assembly procedure is similar for all type of boards and it is described once only. 



### Material list 

| Tools        | Common materials | Board specific materials (see pictures) |
| ------------ | ---------------- | --------------------------------------- |
| Scissors     | Kapton tape      | Aluminium Front plates                  |
| Pliers       | elma screws      | Plastic links                           |
| screwdrivers | M2.5x8mm screws  | copper plates                           |
|              | M2.5x8mm screws  |                                         |

![image: front plates](../images/rimcrate/front_plates.jpg)

*Aluminium front plates for the 3 different kinds of boards*  



![image: copper cooling plates](../images/rimcrate/copper.jpg)

*Copper cooling plates for the 3 different kinds of boards. Note that the pad trigger copper plate has an extra bar (install it if it is not the case)* 



![image: plastic links](../images/rimcrate/plastic_links.jpg)

*Plastic links for the 3 different kinds of boards* 



### Assembly

<ol start="1">
 <li>Kaptonize the aluminium front plates as shown on the pictures below. The inner edge must be perfectly covered.</li>
</ol>

!!! Explanation
    This step is necessary as some unwanted electrical connections have been found between the PCBs and the rim crate itself. This contact happened because of the front plate touching the PCB connectors and the crate. kapton tape is insulating and avoids this problem.

![image: kapton](../images/rimcrate/kapton.jpg)





<ol start="2">
  <li>Screw the front plate to the copper plates using the plastic links. <b>Countersunk holes should be on the back on the plate</b></li>
</ol>

<center>

| Board  | number of screws | screw type |
| ------ | ---------------- | ---------- |
| Router | 6                | M2.5x8mm   |
| Pad    | 4                | M2.5x8mm   |
|        | 4                | M2.5x10mm  |
| L1DDC  | 4                | M2.5x8mm   |
|        | 4                | M2.5x10mm  |

</center>

![image: copper board assembly](../images/rimcrate/assembled_boards.jpg)

<ol start="3">
  <li>Add the elma screws. Use pliers to bend the black plastic part</li>
</ol>
![image: elma screws](../images/rimcrate/elma.png)

*Unlike shown on this old picture, the elma screws must be added after kaptonization of the front plate. Arm yourself with patience as this is the most tricky part of the assembly process*

### Comments: Kaptonizing assembled routers 

For the routers already assembled without kapton, the front plate can be kaptonized as shown on the picture below. Again, make sure **the inner edge must be perfectly covered. **

![image: kaptonized router](../images/rimcrate/router_kap.jpg)



---

## PCB assembly

This section describes the assembly of the PCB onto the copper plates for one rim crate.

### ESD safety

The PCB are sensitive to electrostatic discharges, respect the following measures at all time 

* The PCB (mounted and unmounted) must but stored in a **ESD safe bag** (grey plastic) and carried in the ESD safe back plastic box 

* **Ground yourself** using an ESD bracelet when handling the electronic  boards (when assembling them, when mounting them on the crate, etc...) 

* Work on the **ESD safe matt** when assembling the boards



### Preparation

Cut the thermal pads needed using the metallic ruler and utility knife. For one crate you will need:

* **L1DDC:**

| Component         | size      | thickness | number of pads/crate |
| ----------------- | --------- | --------- | -------------------- |
| U1                | 6x6mm     | 3mm       | x1                   |
| U6                | 6.5x4.4mm | 3mm       | x1                   |
| U7                | 4x4mm     | 3mm       | x1                   |
| SCA               | 12x12mm   | 2.5mm     | x2                   |
| GBTX              | 16x16 mm  | 1mm       | x2                   |
| screws top (VTRX) | 5x5mm     | 1mm       | x2                   |
| screws bottom     | 10x10mm   | 1mm       | x3                   |
| FEASTs            | 10x10mm   | 0.5mm     | x4                   |

* **Pad Trigger:**

| Component | size     | thickness | number of pads/crate |
| --------- | -------- | --------- | -------------------- |
| Repeater  | 8x8mm    | 3mm       | x6                   |
| Fan-out   | 5x5mm    | 2.5mm     | x1                   |
| SCA       | 12x12mm  | 2.5mm     | x1                   |
| FPGA      | 31x31mm  | 0.5mm     | x1                   |
| FEASTs    | 150x10mm | 0.5mm     | x1                   |

* **Router:**

!!! Warning
    The router requires stacking pads

| Component | size     | thickness | number of pads/crate |
| --------- | -------- | --------- | -------------------- |
| FEAST     | 145x20mm | 1.5mm     | x8                   |
| "         | "        | 2.5mm     | x8                   |
| Fiber     | 40x45mm  | 1.5mm     | x8                   |
| "         | "        | 2.5mm     | x8                   |
| FPGA      | 35x35mm  | 1.5mm     | x8                   |

![image: thermal pads cutting example](../images/rimcrate/pad_layout.jpg)

### PCB assembly

**Summary table:** To assemble one of each board you will need: a PCB, the corresponding copper cooling plate and [thermal pads as described above](#preparation) and the following pieces:

| Router                     | L1DDC                    | Pad Trigger                      |
| -------------------------- | ------------------------ | -------------------------------- |
| M3x12mm screws (x8)        | M3x12mm screws (x5)      | M3x12mm screws  (x7)             |
| M3 nuts (x8)               | M3 nuts (x5)             | M3x20mm screw (x1)               |
| 3.4mm plastic spacers (x6) | 3mm plastic washers (x5) | M3 nuts (x8)                     |
| 4mm plastic spacers (x2  ) | brass screws (x3)        | 3mm plastic washer               |
|                            | copper bar               | cooling gallow (2 subpieces, x1) |
|                            |                          | M2.5x4mm screws (x2)             |
|                            |                          | M2.5x8mm screws (x2)             |
|                            |                          | thermal paste                    |



**Router: **

* Place the thermal pads on the corresponding components. Stack 1.5 and 2.5mm pads for the fibers and FEASTs 

![image: router thermal pads layout](../images/rimcrate/router_pads.jpg)

*  Screw the PCB onto the router copper plate using: 

  * M3x12mm screws (x8 per board) 

  * M3 nuts (x8 per board) 

  * 3.4mm plastic washers (x6 per board) 
  
  * 4mm plastic washers (x2 per board) 

  Insert screws from below the copper plate, add spacer (the two 4mm plastic washers are for the two screws in the middle of the router), then PCB then nut. **Do not tighten the screws too much or you will bend the PCB**. Make sure that the PCB is not bent and that and the thermal pads are in good contact once you are done.

![image: how to put  screw+spacer+bolt](../images/rimcrate/screws.jpg)

* Write down the router ID on the front plate using a permanent marker (3 last digit in `RTGC_0***`)

![image: router ID](../images/rimcrate/routerID.jpg)

**L1DDC: **

* **Write down the L1DDC ID on the front plate and in the database:**  [https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit?usp=sharing)

* Put the pads on the fibers then screw the copper bar with the brass screws (inserted from below the PCB) 

![image: cooling of L1DDC fibers](../images/rimcrate/L1fiber.jpg)

* Place the thermal pads on the corresponding components (U1, U6 & U7 : use 3mm pads).  

 ![image: L1DDC thermal pads](../images/rimcrate/L1DDC_pad.jpg)

* Screw the PCB onto the router copper plate using: 
  * M3x12mm screws (x5 per board) 
  * M3 nuts (x5 per board) 
  * 3mm plastic washers (x5 per board) 

Insert screws from below the copper plate, add spacer, then PCB then nut.  **Do not tighten the screws too much or you will bend the PCB**. Make sure that the PCB is not bent and that and the thermal pads are in good contact once you are done.



**Pad trigger:**

* **Write down the L1DDC ID on the front plate and in the database:**    [https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit?usp=sharing)

* Put jumpers on the pins close to the power connectors excepted the 2.5V and GND ones 

 ![image: pad trigger jumpers](../images/rimcrate/jumper.jpg)

* Place the thermal pads on the corresponding components.  

 ![image: pad trigger thermal pads](../images/rimcrate/padTrigger_pads.jpg)

* Put the pads on the fiber (as for the L1 board)

* The pad trigger as a **copper cooling piece** to cool the fiber connector  from the top. Place the bottom piece onto the  copper plate (**using thermal paste**). Insert the PCB. Then screw (M2.5  4mm) the bottom piece to the copper plate. Finally screw (**M2.5 8mm**)the  top part (using thermal paste again).

 ![image: pad trigger gallow](../images/rimcrate/gallow.jpeg)

* Screw the PCB onto the router copper plate using the following. **Warning** one screw is different than the others: the holes have been badly designed and one of the bolts would touch some resistances. For that hole (see picture below) use a 20mm screw and add a plastic spacer on top of the PCB before the bolt. By doing that, nothing should be pressing on the resistances. 
  * M3x12mm screws  
  * **M3x20mm screw**
  * M3 nuts  
  * 3mm plastic washer

Insert screws from below the copper plate, add spacer, then PCB then nut.  **Do not tighten the screws too much or you will bend the PCB**. Make sure that the PCB is not bent and that and the thermal pads are in good contact once you are done.

 ![image: special long screw for pad trigger](../images/rimcrate/pad-screw.jpg)

---

## Rim crate assembly on the wheel

The assembly on the wheel must be done in cooperation with the service team. Plan the installation day and time ahead.

!!! Warning
    This section needs to be updated as procedure was not final upon redaction



### Twinax preparation 

Some twinax come from the sector (long twinax to the routers `J1, J2, J3`, and 6 cables to the `IN 0 to 5` connectors of the pad trigger). They must be routed by the service team up to close to the rim crate. The 24 twinax going from the LVD6R repeater to the pad trigger ( connectors `OUT 0 to 23`) have to be added in 191.



### Rim crate connection

1. Check that the LVDB is installed 
2. Apply thermal paste generously in all the slits of the rim crate 
3. Label the power cables and all the 0.5 twinax cables ([see label list below](#label-lists)) 
4. Pre-connect all the 0.5 twinax cables on the L1DDC 
5. Insert all the boards into the crate. Take note of the MTF IDs of each  board and of the corresponding slot number. The slot numbering follow  ATLS convention along phi and z. [https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit#gid=0](https://docs.google.com/spreadsheets/d/1Qqe-DskVccsPdnoqSQz2D2x4Xj5JjJtpXzPBQiqjm_g/edit#gid=0)
6. Screw the L1DDC and the routers in. They must be tight to ensure good thermal conductivity 
7. Label (permanent marker) the routers (R1 to R8) and the connectors (J1, J2, PRI, AUX, J3 from top to bottom) to avoid confusion when labelling. !Note! : due to the L1 and Pad board the routers 5 to 8 are in the lots 7 to 10 respectively 
8. Service team: connects and routes router cables to J1, J2, J3 
9. With service team: connect pad trigger 
10.  Connect fibers to star modules (routers and pad trigger) and to the fiber boxes (L1DDC) using mapping here: [https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/Fibres/Cable%20and%20box%20Mapping%20docs/0NSW%20Fiber%20mapping.xlsx&action=default]( https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/Fibres/Cable%20and%20box%20Mapping%20docs/0NSW%20Fiber%20mapping.xlsx&action=default)
![image: star module and mapping](../images/rimcrate/starmodule1B.jpg)
11. Update ATLAS database with the parenting information

### Label lists

* **Twinax for internal connections**

|      | **0.5m** **twinax** **labels** |
| ---- | ------------------------------ |
| 1    | R1 PRI                         |
| 2    | R2 PRI                         |
| 3    | R3 PRI                         |
| 4    | R4 PRI                         |
| 5    | R5 PRI                         |
| 6    | R6 PRI                         |
| 7    | R7 PRI                         |
| 8    | R8 PRI                         |
| 9    | R1 AUX                         |
| 10   | R2 AUX                         |
| 11   | R3 AUX                         |
| 12   | R4 AUX                         |
| 13   | R5 AUX                         |
| 14   | R6 AUX                         |
| 15   | R7 AUX                         |
| 16   | R8 AUX                         |
| 17   | PAD PRI                        |
| 18   | PAD AUX                        |

* **Labels for power cables**

|      | **Power cables**    | **comments**                                      |
| ---- | ------------------- | ------------------------------------------------- |
| 1    | Rim-LVDB-EILA12-ch2 | Update number to sector  ch2 goes on LVDB group 1 |
| 2    | LVDB-R1             |                                                   |
| 3    | LVDB-R2             |                                                   |
| 4    | LVDB-R3             |                                                   |
| 5    | LVDB-R4             |                                                   |
| 6    | LVDB-R5             |                                                   |
|      |                     | One spare cable unlabelled on 1st bundle          |
| 7    | Rim-LVDB-EILA12-ch1 | Update number to sector  ch1 goes on LVDB group 2 |
| 8    | LVDB-PAD            |                                                   |
| 9    | LVDB-L1DDC-PRI      |                                                   |
| 10   | LVDB-L1DDC-AUX      |                                                   |
| 11   | LVDB-R6             |                                                   |
| 12   | LVDB-R7             |                                                   |
| 13   | LVDB-R8             |                                                   |

* **Fibers / star modules**

| **Router** | **Layer** |
| ---------- | --------- |
| R1         | L1        |
| R2         | L3        |
| R3         | L5        |
| R4         | L7        |
| R5         | L2        |
| R6         | L4        |
| R7         | L6        |
| R8         | L8        |

* Router to layer correspondence: *

* **Fiber mapping:** 

[https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared%20Documents/Fibres/Cable%20and%20box%20Mapping%20docs/0NSW%20Fiber%20mapping.xlsx&action=default](https://espace.cern.ch/NSWCommissioning/_layouts/15/WopiFrame.aspx?sourcedoc=/NSWCommissioning/Shared Documents/Fibres/Cable and box Mapping docs/0NSW Fiber mapping.xlsx&action=default) 
