# Usefull links

## Readout links

* [Rongkun's twiki](https://twiki.cern.ch/twiki/bin/view/Sandbox/Rongkun191STGC)
* [ICS documantation](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20how%20to.docx?Web=1 )
* [VMM specification](https://twiki.cern.ch/twiki/pub/Atlas/NSWelectronics/vmm3a.pdf)
* [Baseline results online](http://stgc-trimmer.web.cern.ch/stgc-trimmer/)
* [Baseline plotting code on gitlab](https://gitlab.cern.ch/patmasid/stgc_baseline_noise_measurement)
* [Pulser - quick.sh documentation](https://gitlab.cern.ch/rowang/nswutilities/-/tree/master/quickAnalysis_minitree)


## Git repositories
* [https://:@gitlab.cern.ch/atlas-muon-nsw-daq/config-files.git](https://:@gitlab.cern.ch/atlas-muon-nsw-daq/config-files.git)
* [https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/stgc-baseline-trimmer-scripts.git](https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/stgc-baseline-trimmer-scripts.git)
* [https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/NSW_OKS_Test_DB.git](https://:@gitlab.cern.ch:8443/atlas-muon-nsw-daq/NSW_OKS_Test_DB.git)
* [https://:@gitlab.cern.ch:8443/rowang/nswutilities.git](https://:@gitlab.cern.ch:8443/rowang/nswutilities.git)


## Other
* [Printer installation at CERN (linux)](https://linux.web.cern.ch/docs/printing/)