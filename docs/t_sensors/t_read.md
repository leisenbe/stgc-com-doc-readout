In order to read the Temperatures from the T sensors you need to log in into *pccanbustest* with the user *user191* 
```
ssh -XY user191@pccanbustest
```
then run the WCCOA panel

```
source /localdisk/sTGCTemperaturesSYY.sh
```
where <span style="color: red;">YY</span> is the sector number you want to see.

![image] (../../images/t_sensors_A12.png)

:
