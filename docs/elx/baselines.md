#How to take baseline measurements

 
!!! Warning
    Never leave LV unattended and always monitor temperature ! 


## Common Readout setup

### Powering the FEBs

The Low Voltage (LV) used to power the FEBs can come from either the temporary power supplies (PS4 and PS5) or the ICS

1. To turn on the LV using the ICS GUI:
```
ssh -XY user191@pccanbustest 
FSM_LV_STGC
```
If it is your first time using the ICS, checkout the full documentation [here](https://stgc-comm-docs.web.cern.ch/LV/lv_dcs/)

More info about the ICS can be found here:
* [Full instruction to run and debug ICS and LV](https://stgc-comm-docs.web.cern.ch/LV/lv_dcs/)
* [ICS how to (obsolete?)](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20how%20to.docx?Web=1) 
* [ICS mapping](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20channel%20mapping%20dynamic.xlsx?Web=1)


### General Setup
* Power the FEB (see previous section)
* Start and configure FELIX 05. In case of issues checkout the debuging section
```
ssh -XY nswdaq@pcatlnswfelix05 
```
Password: `M***8***!`   
you can aslo use `kinit nswdaq` to avoid typing the password all the time.

Just to be sure, performing soft GBT reset, then check that you can see all the links on the felix:
```
flx-init -c 0 
flx-init -c 1 
flx-info GBT -c 0 
flx-info GBT -c 1 
```
You can also check the GBT status online here: 
[https://nsw191.web.cern.ch/nsw191/check.txt](https://nsw191.web.cern.ch/nsw191/check.txt)
You should see:  
**Samll sector configuration**  
![image Small sector flx-info result](../images/general_setup/flx-info.png)

**Large sector configuration**  
![image Large sector flx-info result](../images/baseline/LargeSectorflx-info.png)

Check the alignment and compare it to the screenshots. In case of problems, power cycle the low voltage/ICS.
 
* Configure elinks:  
We now have two configurations possible: Small sector and Large sector. Choose the appropriate options for your run
To configure the elinks run
```
elinkconfig_sTGC_191 <sector option>
```
Where the `<sector option>` can be:
 * Small sector: `HOIP3_SS`
 * Large Sector: `HOIP3_LS`
 * Both sectors running: `HOIP3_SS_LS`


Initiatlize GBTx1:   
```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/191_sTGC_A14
```
For baselines:
```
./config_gbtx1.py -i # old
./config_gbtx1_LS.py -i # for Large sector configuration
```
For Noise runs and pulser tests:

```
./config_gbtx1.py
```

* Now, to configure the second GBTX, since the fibers are not connected, we do it through the first GBTx using a special script. For this, we need felixcore running. Start felixcore:   
```
felixcore_191_sTGC_HOIP_new 
```
Leave that terminal running with felixcore, check it regularly, if it crashes restart it by repeating the command.  
In a new felix05 terminal initialize GBTx2:
```
ssh -XY nswdaq@pcatlnswfelix05
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/191_sTGC_A14 
./config_gbtx2.py -i # old
./config_gbtx2_LS_card0.py -i
./config_gbtx2_LS_card1.py -i
```
If you see the following error, see box below. Don't continue until this is solved  
```
terminate called after throwing an instance of 'Sca::NoReplyException'
  what():  At /afs/cern.ch/user/p/ptzanis/ScaSoftware/Sca/SynchronousService.cpp:165 in std::vector<Sca::Reply> Sca::SynchronousService::SynchronousChannel::sendAndWaitReply(std::vector<Sca::Request>&, unsigned int) reply hasnt come, #requests=1 #received=0
```

!!! GBTX2 bug
	Felixcore has trouble configuring too many links one after the other and is prone to crashing, and you might need to redo it 2 or 3 times for it to work, I.e. restarting felixcore, then trying to configure the GBTx2 again. Another possibility is to run two scripts, where we have split this second GBTx into two cards to avoid crashes:
	``` 
	./config_gbtx2_card0.py -i
	```   
	```
	./config_gbtx2_card1.py -i 
	```
 

* Once the 2 GBTX configured, start the OPCServer on felix05: 
```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/191/A14 
OpcUaScaServer sector_A14_Uncalibration_STG_191_FlxId1.xml 

OpcUaScaServer --opcua_backend_config /opt/OpcUaScaServer/bin/ServerConfig_LS.xml A14_STG_191_as_Large_Sector_FlxId5.xml 
```

!!! Conclusion
	Congratulation, you are done with the general setup. Please ensure that you started the temperature monitoring. You can now:

	* Take baseline
	* Take pulser data
	* Take a noise run
	* Run the trimmers


## Baselines
### Run
* Open a session in a new SWROD machine: 
```
ssh -Y nswdaq@pcatlnswswrod03 
cd /eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW 
source setup.sh 
```

* To choose which FEBs you want to take baseline for, modify the baseline script as follows:
  ```
  ./scripts/stgc_baselines.sh <sector> <IP/HO> <config JSON>
  ```
  where the options are:

    * `<sector>` The sector you are running with, in the format `A12` or `C01`. This option is required as the VMM configuration of A12 and A14 is different from default. This will load the right FEBs list
    * `<IP/HO>`: script can run on one wedge at a time, chose `IP` or `HO`
    * `<config JSON>` You can specify the `JSON` file to use. If not the `defaultCONFIG` file is used


!!! stgc_baselines

	The script will:  

	1. configure the frontends   
	1. readout digitized values from output monitor digitized by onboard SCA chip.   
	1. run some basic analysis/plotting scripts (maintained by Prachi), but the last processing step results in a core dump - but no worry the baseline were still recorded properly   

* Look at results, they can be visualized at:  

    - [http://stgc-trimmer.web.cern.ch/stgc-trimmer/](http://stgc-trimmer.web.cern.ch/stgc-trimmer/)
it will be the last two folders at the very bottom of the page 
    - Plotting documentation/code:  
[https://gitlab.cern.ch/patmasid/stgc_baseline_noise_measurement](https://gitlab.cern.ch/patmasid/stgc_baseline_noise_measurement)

* Please edit the legend in the document:
  ```
  /eos/atlas/atlascerngroupdisk/det-nsw-stgc/trimmers/191.html
  ```

### Making baseline plots

Baseline and noise plots can be made by using the script in the Dev-JM branch of [stgc_baseline_noise_measurement](https://gitlab.cern.ch/patmasid/stgc_baseline_noise_measurement/-/tree/Dev-JM)

If this is the first time using the repository follow the directions below from an empty directory to pull and
run, if not skip to the next block.
```
mkdir source build
cd source
git clone <url >
git checkout Dev-JM
source ./stgc_baseline_noise_measurement/setupEnv.sh
printf "cmake_minimum_required(VERSION 3.4.3)\nfind_package(TDAQ)\ninclude(CTest)\ntdaq_project(NSWDAQ 1.0.0 USES tdaq 8.2.0)\n" > CMakeLists.txt
cd ../build
cmake ../source
make
cd stgc_baseline_noise_measurement
./analyzeHit (5 arguments)
```

If the repository is already cloned and on the Dev-JM branch

```
cd source/stgc_baseline_noise_measurement
git pull //to bring branch up to date
cd ../../build
source ../source/stgc_baseline_noise_measurement/setupEnv.sh
cmake ../source
make
cd stgc_baseline_noise_measurement
./analyzeHit (5 arguments)
```

The arguments to the executable are as follows:

* path to input summary-txt file (`summary_baselines.txt`)
* path to input baselines-txt file (`baselines.txt`)
* output file path (with `'/'` at the end)
* SmallOrLarge (`'S'` or `'L'`)
* PivotOrConfirm (`'P'` or `'C'`)

Two measurements can be compared using the compare script which is run on the output of stgc_baseline_noiseMeasurement. To run:
```
cd source/stgc_baseline_noise_measurement/script/

g++ compare.cxx -o compare -iquote ../ `root-config --cflags --glibs`

./compare summary_plots1.root summary_plots2.root name1 name2 outputfile.pdf IPorHO SorL
```

## Masking channels


The baseline results will show dead channels and noisy channels that need to be masked. You need to  identify the channels to mask and then generate a json configuration file that will maske the required channels.  
To generate a json file with the right channels masked:

 * Run IP and HO baselines, you will need the results to identify noisy/dead channels
 * Results of baseline run should be in: `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW/trimmers_sTGC/<your baseline run>`
 * Noisy channels are characterised by a high stdev above ~10mV. They are identified using `summary_baselines.txt`. Note that the values in that file are not in mV and need to be converted. `(ADC*1000.*4095.=mV)`
 * Dead channels have a mean count below 150mV, channels with a high count above 200mV are masked as well. These channels are identified using `baseline_outside_150_200mV_2.txt`
 * The script generates a new json file from the template with masked channels

!!! Important
	The scripts to use are available and documented here: `https://gitlab.cern.ch/acanesse/stgc_mask_channels`
### run channel masking script
1. Prepare list of channels to mask:

In the IP baseline results folder run:
```
awk '{if ($10>50){ print $2,$3,$4,$5,$6,$10}}' summary_baselines.txt | tee tomaskIP.txt 
awk '{if ($8<145 || $8>200){ print $2,$3,$4,$5,$6,$8}}' baseline_outside_150_200mV_2.txt |tee -a tomaskIP.txt 
```
Note that 10mV = 40.95 ADC, the threshold for `summary_baseline` is in ADc counts whereas the threshold for `baseline_outside_150_200mV_2` is in mV.
Repeat for HO side
```
awk '{if ($10>50){ print $2,$3,$4,$5,$6,$10}}' summary_baselines.txt | tee tomaskHO.txt 
awk '{if ($8<145 || $8>200){ print $2,$3,$4,$5,$6,$8}}' baseline_outside_150_200mV_2.txt |tee -a tomaskHO.txt 
```
Combine files:
```
cat tomaskIP.txt > tomask.txt && cat tomaskHO.txt >> tomask.txt 
```

2. Update input file `tomask.txt` in `mask_channels.sh` if need be and output jsonfile name

3. To generate the masked json, run the pytion script (will not work on felix05!): 
```
ssh -XY nswdaq@pcatlnswswrod03 
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A14 
./mask_channels.sh
```
Update input file in the bash script to your list of channels to mask. Update output json file name. 


## Temperature monitoring
Two monitoring systems are possible:  

* Using the T-sensors on the wedge: can run anytime but do not give the ectronics temperature
* VMM temperatures: actual VMM temperature but requires OPC server to run

### T-sensors:
They can now be accessed online at: [https://nsw191.web.cern.ch/nsw191/temperature/](https://nsw191.web.cern.ch/nsw191/temperature/)

Or locally, using:
```
ssh -XY user191@pccanbustest 
cd /localdisk 
source sTGCTemperaturesSXX.sh 
```
(where XX is the sector number)

### VMM temperature

Start DCS GUI to monitor temperature, etc on FEB 
```
ssh -XY user191@pccanbustest 
```
Passwd: u******1 
```
cd /localdisk 
./STGSCA 
```

Note that to also see the VMM temperatures you must have the following registers:   
sm=4, scmx=0, sbmx=1, sbft=0, sbfp=1, sbfm=0) 

## Debuging
### Recover vmms
If some vmms appear masked when they shouldn't be, then they should be recovered by modifying the json file. This problem is due to a badly manifactured ground line in the vmm chip.  

 * Identify the FEB and vmm (0 to 7)
 * open json file (adapt to sector)  
  `/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/STGC_191_HOIP_baselines_as_LS.json`
 * add the following line in the right vmm configuration block to enable the monitor output:
 ```
 "sbfm": 1
 ```

Example of unwanted "masked" VMM. The procedure should be applied to sFEB_L3Q3_HOL vmm5 in this case
![image masked vmm](../images/baseline/maskedVmm.png)


### Starting FELIX after shutdown
If the felix machines have been turned off (e.g power cut), it might be necessary to reprogram the boards and setup the drivers. If FELIX wasn't shutdown, skip this section.

1. Connect the Digilent programmer (USB-to-JTAG) hanging from the FELIX backplane to the FELIX FPGA board that is connected to the motherboard's PCI express slot. On the very end of the FELIX FPGA board there is an associated connector for this  

2. Run this command in the FELIX PC (as a NSW user):  
```
ssh -XY nswdaq@pcatlnswfelix05 
progFELIX
```

3. Repeat steps 1-3 for the second felix card 

4. Reboot the computer once the commands above have finished.
```
reboot
```

5. Setup the drivers to be able to see the felix cards: 
```
sudo /etc/init.d/drivers_flx start 
```
Now you should be able to run flx-init and other commands like that. 


### Check running processes: 

If you get errors, you first need to check if there are no left over processes running in the background: 
```
hey
```
Or the more classic:
```
ps aux | grep Opc  
```
and then kill undesired processes 
```
ps aux | grep felixcore  
pkill -9 -c -f felixcore 
```
If a board seems to give problems regardless of what you try, sometimes reprogramming the felix cards solves the issue. 

### Problem with flx-init
If seeing a lot of:
```
LOL register = 0x02
```
When running `flx-init`, check that the ATLI leds are `ON`. If `OFF` reconfugure ALTI as indicated below.
 

### ALTI: 

To check if Alti is configured correctly, log on to sbc:   
```
ssh -Y nswdaq@sbcl1ct-191-1  
setupSBC  
menuAltiModule 
2 ["CFG" menu] Global configuration 
3 check ALTI 
```
You should get something like this: 
```
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - CLK mux = "SETUP" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - I2C core = "SETUP" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - PLL = "LOCKED" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - jitter cleaner design = "ALTI_001" (DEFAULT) 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - ADER1 page register = "SETUP" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - TTC encoder = "SETUP" 
```

If not, you can do: 
```
 2 ["CFG" menu] Global configuration 
 1 reset ALTI 
 2 setup ALTI 
 3 check ALTI 
```

Another problem that might happen is that there are no patterns being sent from the ALTI. This can be seen by looking at the front plate of the ALTI;  if the L1A light is ON, this means that patterns are being sent (for example after sTGC trigger link tests). 

This can also be checked without looking at the LEDs in the alti menu by doing: 
```
 9 ["ENC" menu] TTC encoder 
 1 transmitters status 
```

You should get something like this: 

 

The two top ones are the important ones. If they are disabled, you can do: 
```
 9 ["ENC" menu] TTC encoder 
 2 transmitters enable 
 Transmitter (0="TX0", 1="TX1", ..., "10=TX10", 11=all) [0..11]: 0 Enable (0=disable, 1=enable) [0..1]: 1 
 Transmitter (0="TX0", 1="TX1", ..., "10=TX10", 11=all) [0..11]: 1 Enable (0=disable, 1=enable) [0..1]: 1 
```
 
If everything on Alti looks fine and the events still look empty, make sure the pattern is enabled: in the setup file (setup_oks_NSW-191-TGC-SWROD.sh) one finds the "partition" file the script is using (in this case muons/partitions/part_NSW-191-TGC-SWROD.data.xml). This file in turn, points to various "segments", one of these being the alti segment (vim muons/segments/NSW/NSW-Alti-191-TGC.data.xml)


 
