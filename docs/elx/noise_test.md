# How to take Noise Runs

!!! Warning

Please never leave LV unattended and always monitor the ICS temperatures !

## Setup

### Powering ON FEBs

The Low Voltage (LV) used to power the FEBs can come from either the temporary power supplies (PS4 and PS5) or the ICS

1. To turn on the LV using the ICS GUI:
```
ssh -XY user191@pccanbustest 
source /localdisk/winccoa/ATLSTGLV/runfsmsTGC.sh 
```

More info about the ICS can be found here:

* [ICS how to](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20how%20to.docx?Web=1) 
* [ICS mapping](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20channel%20mapping%20dynamic.xlsx?Web=1)


## Preparing the files

To run noise tests, you need a completed json file with the correct trimmers for each channel. However, trimmer tests are run on IP and HO separately, which means that we need to merge the json files that were produced. 

 First, open a fresh new felix terminal, and start the felix GUI:
```
ssh -XY nswdaq@pcatlnswfelix05 
source /afs/cern.ch/work/r/rowang/public/FELIX/setup_nsw_board_ttc.sh  
stgc-dcs 
```

Click the folder to open a json file. You need to find the correct run folders here:

```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW/trimmers_sTGC/
```

And load the configuration by clicking "Load Config". The name of the json file should turn green. If you click on the dropdown to select the board (top right, should say common) you will see the boards. You can select one of them and check if it has the correct trimmers.

You will need to delete the boards that do not have a trimmer set. So, if you're looking at the IP file, delete all the HO files, and viceversa. Save those new files. Now, while you have one of them open, activate the "Add Config" box, and now load the second one. If you check the boards again, you should have the full sector, with both IP and HO with their corresponding trimmers. Save this file.

### Lowering the threshold

Now, make a copy of this file for each different noise test you want to do, where each one will have a different threshold. Usually we make 4 extra copies, where each copy has a threshold 20 DAQ units lower than the previous one.

To edit the threshold of the new files, open it with vim and do

```
:g/sdt_dac/norm 20^X
```  

where ^X means CRTL-X, and 20 is how much you are decreasing the threshold.

Make sure that the json files have the registers `STH` and `ST` turned off. To do that, open the files using the Felix GUI, select the `VMM` tab and go to `Channel Registers` (to check that you are editing all boards at the same time make sure `common` is selected on the top right drop-down list). Here, turn off `STH` and `ST` by double clicking their respective buttons. Make sure to save this config by clicking `Write json`, and selecting the same file from before.

### Turning off the pulser pattern

The partition used to take data is defined here:
```
/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/NSW_OKS_Test_DB_191/NSW_OKS_Test_DB/muons/partitions/part_NSW-191-TGC-SWROD.data.xml 
```
The paragraph at line 45 includes more configuration files. The address of the *.dat file that must be edited can be found in `/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/NSW_OKS_Test_DB_191/NSW_OKS_Test_DB/muons/segments/NSW/NSW-Alti-191-TGC.data.xml` under `pattern_filename`.  

Open the pattern file at:
```
/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/ttc_patterns/cont_altiPat_stgc.dat 
```
And comment out the indicated line to turn off the pulse pattern from Alti.

### Running a noise test

Once that has been done, overwrite the dummy file with the json file of the noise test you wish to run using the Felix GUI.

Now run pulser tests as usual, the recommendation would be to run without an event limit, and let the measurement run for a few minutes, or even longer if needed. 

If you need to run regular pulser tests, you must undo the steps from before, i.e. turn ON the STH and ST for all boards and VMMs, and uncomment the line on the Alti pattern file. 
