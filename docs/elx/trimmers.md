### General Setup
* Power the FEB (see previous section)
* Start and configure FELIX 05. In case of issues checkout the debuging section
```
ssh -XY nswdaq@pcatlnswfelix05 
```
Password: `M***8***!`   
you can aslo use `kinit nswdaq` to avoid typing the password all the time.

Just to be sure, performing soft GBT reset, then check that you can see all the links on the felix:
```
flx-init -c 0 
flx-init -c 1 
flx-info GBT -c 0 
flx-info GBT -c 1 
```
You can also check the GBT status online here: 
[https://nsw191.web.cern.ch/nsw191/check.txt](https://nsw191.web.cern.ch/nsw191/check.txt)
You should see:  
**Insert picture**   

Check the alignment and compare it to the screenshots. In case of problems, power cycle the low voltage/ICS.
 
* Configure elinks:  
```
elinkconfig_sTGC_191 HOIP 
```

* Initiatlize GBTx1:   
```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/191_sTGC_A14
```
For baselines:
```
./config_gbtx1.py -i
```
For Noise runs and pulser tests:

```
./config_gbtx1.py
```

* Now, to configure the second GBTX, since the fibers are not connected, we do it through the first GBTx using a special script. For this, we need felixcore running. Start felixcore:   
```
felixcore_191_sTGC_HOIP 
```
Leave that terminal running with felixcore, check it regularly, if it crashes restart it by repeating the command.  
In a new felix05 terminal initialize GBTx2:
```
ssh -XY nswdaq@pcatlnswfelix05
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/191_sTGC_A14 
./config_gbtx2.py -i
```
If you see the following error, see box below. Don't continue until this is solved  
```
terminate called after throwing an instance of 'Sca::NoReplyException'
  what():  At /afs/cern.ch/user/p/ptzanis/ScaSoftware/Sca/SynchronousService.cpp:165 in std::vector<Sca::Reply> Sca::SynchronousService::SynchronousChannel::sendAndWaitReply(std::vector<Sca::Request>&, unsigned int) reply hasnt come, #requests=1 #received=0
```

!!! GBTX2 bug
	Felixcore has trouble configuring too many links one after the other and is prone to crashing, and you might need to redo it 2 or 3 times for it to work, I.e. restarting felixcore, then trying to configure the GBTx2 again. Another possibility is to run two scripts, where we have split this second GBTx into two cards to avoid crashes:
	``` 
	./config_gbtx2_card0.py -i
	```   
	```
	./config_gbtx2_card1.py -i 
	```
 

* Once the 2 GBTX configured, start the OPCServer on felix05: 
```
cd ~/public/sw/config-ttc/config-files/opc_xml/191/A14 
OpcUaScaServer sector_A14_Uncalibration_STG_191_FlxId1.xml 
```

!!! Conclusion
	Congratulation, you are done with the general setup. Please ensure that you started the temperature monitoring. You can now:

	* Take baseline
	* Take pulser data
	* Take a noise run
	* Run the trimmers


# Trimmers/threshold tuning

The different VMM channels have different baseline values and the threshold for signal must be adjusted. To tune the thresholds: 

* Follow the common readout setup  
* Run the threshold tuning: 
```
ssh -Y nswdaq@pcatlnswswrod03 
cd /eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/191_quick_and_dirty_baselines_NEW 
source setup.sh 
```
Edit `stgc_threshold.sh` to choose which FEBs you want to take baseline for. Note that you can only run on IP or HO at a time. Then run the script 
```
./scripts/stgc_threshold.sh S P 
```
Where the options are `S` for small sector `P` for HO and `C` for IP side 

Results can be visualized at:

* [http://stgc-trimmer.web.cern.ch/stgc-trimmer/](http://stgc-trimmer.web.cern.ch/stgc-trimmer/)
* If you need the files themselves, they can be found at: `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/trimmers`

 