# General Setup

#### Preface - Machines and users

Depending on the application, you will be logging into different machines, sometimes using multiple sessions. These are:

1. `pcatlnswfelix05` (or just Felix machine): main machine to communicate with the GBTx boards.
2. `pcatlnswswrod03` (or SWROD): machine you will connect to once you are ready to take data.
3. `pccanbustest`: machine generally used to run monitoring software.

You must log to the Felix and SWROD machines using the `nswdaq` user, whose password is `MMFE8ROC!`.

On the other hand, on pccanbusttest you will log on using `user191`, with the password `user_191`.

All these machines are actual computers on the rack at the back of b191, so if there are any problems, you can go and physically check if everything is on and working.

### Turn on electronics

First of all, turn on the LV on the sector you want to work on:

```
ssh -XY user191@pccanbustest 
FSM_LV_STGC 
```

You will see a screen like this:

![](../images/general_setup/fsm_gui.png)

On this first screen, press the `Quick Module Setup` button on the left to turn on the ICS modules that you will use.

Then, you can use the buttons on the left to navigate to the different parts of the detector, from a particular wheel (`Side A`) , then sector (`Sector N`), then wedge (1 for IP, and 4 for HO), and finally layer (Digital + 1-4 analog). On each of these levels you can press the button that says `NOT_READY` to see a drop down with the orders you can send. Send a `GOTO_ON` order to turn that specific part on.

*MAKE SURE COOLING IS ON BEFORE TURNING ON ANY ELECTRONICS!*

From this UI you can also monitor temperature, voltage and current on each ICS channel, by navigating down to at least wedge level.

### GBTx soft reset

Log onto the felix machine and run the following commands:

```
ssh -XY nswdaq@pcatlnswfelix05
flx-init -c 0
flx-init -c 1
```

This will do a soft reset of the GBTs. Once it finishes, you can check if the channels are aligned going to this link: [https://nsw191.web.cern.ch/nsw191/check.txt](https://nsw191.web.cern.ch/nsw191/check.txt), or by running:

```
flx-info GBT -c 0
flx-init GBT -c 1
```

You should see the following channels properly aligned:

![](../images/general_setup/flx-info.png)

If a link is missing, usually power cycling the boards solves it (turn off and the on again the digital boards using the FSM). If the problem persists, make sure the corresponding fiber is installed and that it hasn't been damaged.

If everything looks in order, run the following command:

```
elinkconfig_sTGC_191 HOIP
```

which will configure the elinks to start comunication with the GBTs.

### GBTx configuration

Once that command finishes run:

```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/191_sTGC_A14 
./config_gbtx1.py -i 
```

This script will configure the GBTx1. In order to configure the GBTx2, since the fiber isn't connected yet, we need to configure it *through* the GBTx1 using felixcore.

```
felixcore_191_sTGC_HOIP 
```
You can rename this terminal `felixcore`, since it will be running it continually, and you will need to open another terminar to continue working.


So, open a new terminal and log into `nswfelix05` again. Then go to the same directory we were in and configure the GBTx2:

```
ssh -XY nswdaq@pcatlnswfelix05
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/191_sTGC_A14
./config_gbtx2.py -i
```

This code might make felixcore crash. If that happens, just go to your felixcore terminal, restart felixcore, and retry. You don't need to configure the GBTx1 again.

### Starting the OPC Server

After the second configuration has been run without problems, you'll have to start the OPC Server:

```
cd ~/public/sw/config-ttc/config-files/opc_xml/191/A14
OpcUaScaServer sector_A14_Uncalibration_STG_191_FlxId1.xml
```

It should take about a minute to start the OPC Server, but once it starts, it should stay "stable", meaning you wouldn't get any new lines. If it's constantly writing new lines to the terminal, then it's "unstable". Check if felixcore hasn't crashed, and that all the channels are still aligned. If it's unstable, you might have to retry from the beginning, but if not, congratulations! You are almost ready to start taking data. You can rename this terminal "OPC Server", since it will have to run it continually, and any work you do needs to be on a new terminal.

### Temperature monitoring

Open a new terminal, log onto pccanbustest and run the following command:

```
ssh -XY user191@pccanbustest
STGSCA
```

This will open the GUI for the SCA temperature monitor. Keep an eye on this while taking data to make sure the boards are being cooled properly.

![](../images/general_setup/sca_temp_monitor.png)

If a board looks red and the temperature isn't updating, don't panic yet: that only means that the connection has been lost. Take a look at https://nsw191.web.cern.ch/nsw191/check.txt to make sure that's the case, and restart the system and try again.

On the same pccanbustest session, run:

```
source /localdisk/sTGCTemperaturesSXX.sh
```

where XX is the number of the sector you're working on (1-16). This will open the GUI for the T-Sensor monitor. It shows the temperature recorded by the T-Sensors on the sector, as well as the temperatures of the input/output on the cooling pipes.

![](../images/general_setup/tsensors_gui.png)

And with that you are ready to take data! Keep an eye on felixcore and the OPC Server, because they might crash during readout, and you'd have to close everything and start again.
