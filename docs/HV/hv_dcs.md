# HV control

To turn on the HV, log in to pccanbustest and launch the HV FSM using the alias below
```
ssh -XY pccanbustest
FSM_HV_STGC
```
Click on `side A` to display the 2 sectors. Next to sector 1 or 2, its state is defined, you can click on it and then go to `go to on` or `go to off` as needed

![image: HV FSM screenshot](https://gitlab.cern.ch/gvasquez/stgc-com-doc-readout/-/raw/master/images/HV-LV/HVfsm.png)
